package com.local.zklochkov.mvvmlernproject.views;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.local.zklochkov.mvvmlernproject.R;
import com.local.zklochkov.mvvmlernproject.databinding.ActivityMainBinding;
import com.local.zklochkov.mvvmlernproject.models.Demo;
import com.local.zklochkov.mvvmlernproject.models.User;
import com.local.zklochkov.mvvmlernproject.viewModels.ProfileViewModel;

public class MainActivity extends AppCompatActivity {

    private static final int LAYOUT_ACTIVITY = R.layout.activity_main;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        ActivityMainBinding binding = DataBindingUtil.setContentView(this, LAYOUT_ACTIVITY);

        User user = Demo.getUser();
        binding.setViewModel(new ProfileViewModel(this, user));
    }
}
