package com.local.zklochkov.mvvmlernproject.models;

public class Counters {
    private int photos;
    private int friends;
    private int stars;

    public Counters(int photos, int friends, int stars) {
        this.photos = photos;
        this.friends = friends;
        this.stars = stars;
    }

    public int getPhotos() {
        return photos;
    }

    public void setPhotos(int photos) {
        this.photos = photos;
    }

    public int getFriends() {
        return friends;
    }

    public void setFriends(int friends) {
        this.friends = friends;
    }

    public int getStars() {
        return stars;
    }

    public void setStars(int stars) {
        this.stars = stars;
    }
}
