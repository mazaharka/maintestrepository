package com.local.zklochkov.mvvmlernproject;

import android.databinding.BindingAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.local.zklochkov.mvvmlernproject.utils.CircleTransform;
import com.squareup.picasso.Transformation;

public class BindingAdapters {
    private BindingAdapters() {
        throw new AssertionError();
    }

    @BindingAdapter({"android:src"})
    public static void loadImage(ImageView view, String url) {

        Transformation transformation = new CircleTransform();

//        Picasso.get()
//                .load(url)
//                .transform(transformation)
//                .into(view);

        Glide.with(view.getContext())
                .load(url)
                .circleCrop()
                .into(view);
    }
}
