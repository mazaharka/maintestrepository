package com.local.zklochkov.mvvmlernproject.variables;

import android.databinding.BaseObservable;
import android.databinding.BindingConversion;
import android.view.View;

public class ObservableBoolean extends BaseObservable {

    private boolean value;

    public ObservableBoolean(boolean value) {
        this.value = value;
    }

    public ObservableBoolean() {
    }

    @BindingConversion
    public static boolean convertToBoolean(ObservableBoolean observableBoolean) {
        return observableBoolean.get();
    }

    public boolean get() {
        return value;
    }

    public void set(boolean value) {
        if (this.value != value) {
            this.value = value;
            notifyChange();
        }
    }
}
