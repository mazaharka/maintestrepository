package com.local.zklochkov.mvvmlernproject.viewModels;

import android.content.Context;
import android.databinding.BaseObservable;
import android.view.View;

import com.local.zklochkov.mvvmlernproject.models.Demo;
import com.local.zklochkov.mvvmlernproject.models.User;
import com.local.zklochkov.mvvmlernproject.utils.RecyclerConfiguration;
import com.local.zklochkov.mvvmlernproject.variables.ObservableBoolean;
import com.local.zklochkov.mvvmlernproject.variables.ObservableInt;
import com.local.zklochkov.mvvmlernproject.variables.ObservableString;

public class ProfileViewModel extends BaseObservable {

    public static final int LOADING_SHORT = 1000;

    public final ObservableString status = new ObservableString();
    public final ObservableBoolean isFriend = new ObservableBoolean();
    public final ObservableInt starsCount = new ObservableInt();
    public final RecyclerConfiguration recyclerConfiguration = new RecyclerConfiguration();
    public final ObservableBoolean isLoaded = new ObservableBoolean(true);
    private final ObservableBoolean isOnline = new ObservableBoolean();
    private final ObservableInt friendsCount = new ObservableInt();
    public User user;

    private Context context;

    public ProfileViewModel(Context context, User user) {
        this.context = context;
        this.user = user;

        status.set(user.getStatus());
        isOnline.set(user.isOnline());
        isFriend.set(user.isFriend());
//        friendsCount.set(user.getCounters().getFriends());
//        starsCount.set(user.getCounters().getStars());
    }

    public void onClickChangeFriendshipStatus(View view) {
        load(() -> {
            if (isFriend.get()) friendsCount.decrement();
            else friendsCount.increment();
            isFriend.set(!isFriend.get());
        });

    }

    private void load(Runnable onLoaded) {
        isLoaded.set(false);
        Demo.simulateLoading(() -> {
            onLoaded.run();
            isLoaded.set(true);
        }, false);
    }

    private void stopLoad(Runnable onLoaded){

    }

    public void onResume() {
    }
}
